from django.contrib import admin
from . import models


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("name", "funded", "pledged", "backers_count", "category_name")
    date_hierarchy = "created_at"
    fields = ("name", "funded", "pledged", "backers_count", "category_name")

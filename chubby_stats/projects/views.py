from django.shortcuts import render
from django.db.models.functions import Extract
from django.db import models

from .models import Project


@models.DateTimeField.register_lookup
class ExtractYear(Extract):
    lookup_name = "YEAR"


@models.DateTimeField.register_lookup
class ExtractMonth(Extract):
    lookup_name = "MONTH"


@models.DateTimeField.register_lookup
class ExtractWeek(Extract):
    lookup_name = "WEEK"


def get_queryset(params):
    # Period
    date_from = params.get("date_from", None)
    date_to = params.get("date_to", None)
    # Index (num_projects, pledged, num_backers)
    index = params.get("index", "num_projects")
    # Agg Period (YEAR, MONTH, WEEK, DAY)
    agg = params.get("agg", "WEEK")
    # Filters
    # - state
    # - category
    state = params.get("state", None)
    category = params.get("category", None)

    # Make the query
    qs = Project.objects.all()
    values = []
    group_by = []
    if state:
        qs = qs.filter(state=state)
    if category:
        qs = qs.filter(category_name=category)
    if date_from:
        qs = qs.filter(launched_at__gte=date_from)
    if date_to:
        qs = qs.filter(launched_at__lte=date_to)
    if agg == "YEAR":
        qs = qs.annotate(year=ExtractYear("launched_at"))
        values.append("year")
        group_by.append("year")
    elif agg == "MONTH":
        qs = qs.annotate(year=ExtractYear("launched_at"))
        qs = qs.annotate(month=ExtractMonth("launched_at"))
        values.append("year")
        values.append("month")
        group_by.append("year")
        group_by.append("month")
    elif agg == "WEEK":
        qs = qs.annotate(year=ExtractYear("launched_at"))
        qs = qs.annotate(week=ExtractWeek("launched_at"))
        values.append("year")
        values.append("week")
        group_by.append("year")
        group_by.append("week")
    if index == "num_projects":
        qs = qs.annotate(num_projects=models.Count("id"))
        values.append("num_projects")
    elif index == "pledged":
        qs = qs.annotate(sum_pledged=models.Sum("pledged"))
        values.append("sum_pledged")
    elif index == "num_backers":
        qs = qs.annotate(sum_backers=models.Sum("backers_count"))
        values.append("sum_backers")
    qs = qs.order_by(*values)
    qs.query.group_by = group_by
    qs = qs.values(*values)
    return qs


def get_categories():
    return (
        Project.objects.distinct("category_name")
        .order_by("category_name")
        .values("category_name")
    )


def get_statuses():
    return Project.objects.distinct("state").order_by("state").values("state")


def stats(request):
    qs = get_queryset(request.GET)
    # select extract(YEAR FROM launched_at) as year, extract(WEEK FROM launched_at) as week, count(*) from projects_project where funded = true group by year, week order by year, week;
    data = {
        "params": request.GET,
        "qs": qs,
        "statuses": get_statuses(),
        "categories": get_categories(),
    }
    return render(request, "projects/stats.html", data)

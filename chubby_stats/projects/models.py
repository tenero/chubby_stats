from django.db import models


STATUS = [
    ("canceled", "canceled"),
    ("failed", "failed"),
    ("live", "live"),
    ("successful", "successful"),
    ("suspended", "suspended"),
]


class Project(models.Model):

    funded = models.BooleanField()
    external_id = models.IntegerField()
    name = models.CharField(max_length=2000)
    goal = models.DecimalField(max_digits=20, decimal_places=2)
    pledged = models.DecimalField(max_digits=20, decimal_places=2)
    state = models.CharField(max_length=200, choices=STATUS)
    currency = models.CharField(max_length=3)
    deadline = models.DateTimeField()
    created_at = models.DateTimeField()
    launched_at = models.DateTimeField()
    backers_count = models.IntegerField()
    comments_count = models.IntegerField()
    static_usd_rate = models.DecimalField(max_digits=20, decimal_places=2)
    category_name = models.CharField(max_length=200)
    creator_name = models.CharField(max_length=200, null=True)
    category_external_id = models.IntegerField()
    creator_external_id = models.IntegerField(null=True)

    class Meta:
        ordering = ["-created_at"]

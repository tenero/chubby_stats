import os
from django.core.management.base import BaseCommand
from django.db import connections
from contextlib import closing


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("csv_file", nargs="+", type=str)

    def handle(self, *args, **options):
        self.stdout.write("Importing file...")
        fname = os.path.abspath(options["csv_file"][0])
        with closing(connections["default"].cursor()) as cursor:
            # cursor.copy_from(
            #     file=csvfile,
            #     table="projects_project",
            #     sep=",",
            #     columns=(
            #         "funded",
            #         "external_id",
            #         "name",
            #         "goal",
            #         "pledged",
            #         "state",
            #         "currency",
            #         "deadline",
            #         "created_at",
            #         "launched_at",
            #         "backers_count",
            #         "comments_count",
            #         "static_usd_rate",
            #         "category_name",
            #         "creator_name",
            #         "category_external_id",
            #         "creator_external_id",
            #     ),
            # )
            cursor.execute(
                'COPY projects_project("funded", "external_id", "name", "goal", "pledged", "state", "currency", "deadline", "created_at", "launched_at", "backers_count", "comments_count", "static_usd_rate", "category_name", "creator_name", "category_external_id", "creator_external_id") FROM %s DELIMITER %s QUOTE %s CSV HEADER;',
                (fname, ",", '"'),
            )
        self.stdout.write("Done")


# \copy projects_project("funded", "external_id", "name", "goal", "pledged", "state", "currency", "deadline", "created_at", "launched_at", "backers_count", "comments_count", "static_usd_rate", "category_name", "creator_name", "category_external_id", "creator_external_id") from export-projects-2019-05-20.csv delimiter ',' quote '"' csv header;

#!/bin/bash
export PGPASSWORD=123456
psql -U devel -h localhost -d chubby_stats <<EOF
CREATE TABLE "projects_project" ("id" serial NOT NULL PRIMARY KEY, "funded" boolean NOT NULL, "external_id" integer NOT NULL, "name" varchar(2000) NOT NULL, "goal" numeric(20, 2) NOT NULL, "pledged" numeric(20, 2) NOT NULL, "state" varchar(200) NOT NULL, "currency" varchar(3) NOT NULL, "deadline" timestamp with time zone NOT NULL, "created_at" timestamp with time zone NOT NULL, "launched_at" timestamp with time zone NOT NULL, "backers_count" integer NOT NULL, "comments_count" integer NOT NULL, "static_usd_rate" numeric(20, 2) NOT NULL, "category_name" varchar(200) NOT NULL, "creator_name" varchar(200) NULL, "category_external_id" integer NOT NULL, "creator_external_id" integer NULL);
EOF

export PGPASSWORD=123456
YEAR=2018
psql -U devel -h localhost -d chubby_stats <<EOF
\copy (\
  SELECT EXTRACT(YEAR FROM "launched_at") as "year", \
         EXTRACT(WEEK FROM "launched_at") as "week", \
         "category_name", \
         SUM("pledged" * "static_usd_rate") \
    FROM "projects_project" \
   WHERE "state" = 'successful' \
     AND EXTRACT(YEAR FROM "launched_at") = '${YEAR}' \
   GROUP BY "year", "week", "category_name" \
   ORDER BY "year", "week", "category_name") \
to '${YEAR}_pledged_week_category.csv' delimiter ',' quote '"' csv header;
EOF

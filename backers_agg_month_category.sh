export PGPASSWORD=123456
YEAR=2016
psql -U devel -h localhost -d chubby_stats <<EOF
\copy (\
  SELECT EXTRACT(YEAR FROM "launched_at") as "year", \
         EXTRACT(MONTH FROM "launched_at") as "month", \
         "category_name", \
         SUM("backers_count") \
    FROM "projects_project" \
   WHERE "state" = 'successful' \
     AND EXTRACT(YEAR FROM "launched_at") = '${YEAR}' \
   GROUP BY "year", "month", "category_name" \
   ORDER BY "year", "month", "category_name") \
to '${YEAR}_backers_month_category.csv' delimiter ',' quote '"' csv header;
EOF

# Chubby bird data analysis

Bash scripts (and a django app) to analyze data from chubby bird.

## Db initialization

For the django app:

    docker-compose up
    python chubby_stats/manage.py migrate
    sh import_csv.sh <filename>

For the script:

    docker-compose up
    sh create_table.sh
    sh import_csv.sh <filename>

## Gettings stats

For the django app:

    python chubby_stats/manage.py runserver

then visit http://localhost:8000/

For the scripts:

    # Total projects grouped by week and category
    count_agg_week_category.sh
    # Total backers grouped by week and category
    backers_agg_week_category.sh
    # Total pledged grouped by week and category
    pledged_agg_week_category.sh

this scripts will save the data in a csv in the current working dir.
You can edit the scripts and change the reference year using the
variable `YEAR`

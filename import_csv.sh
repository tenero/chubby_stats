#!/bin/bash
export PGPASSWORD=123456
psql -U devel -h localhost -d chubby_stats <<EOF
\copy projects_project("funded", "external_id", "name", "goal", "pledged", "state", "currency", "deadline", "created_at", "launched_at", "backers_count", "comments_count", "static_usd_rate", "category_name", "creator_name", "category_external_id", "creator_external_id") from $1 delimiter ',' quote '"' csv header;
EOF
